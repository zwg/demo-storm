### 实时计算Storm

##ACK
https://blog.csdn.net/weiyongle1996/article/details/77397055
    https://blog.csdn.net/tototuzuoquan/article/details/73539952
    https://blog.csdn.net/cm_chenmin/article/details/52925225
    
Storm的ack机制在项目应用中的坑 https://www.cnblogs.com/intsmaze/p/5918087.html

Storm 性能优化
https://www.cnblogs.com/peak-c/p/6297794.html

maxSpoutPending
https://www.jianshu.com/p/0359eeeeba8b

Storm集成 Redis 详解
https://blog.csdn.net/m0_37809146/article/details/91128313


hbase 客户端超时、重连设置  https://www.jianshu.com/p/40704822fec8
HBase之超时机制  https://blog.csdn.net/zhanglh046/article/details/78510313


kafka
window安装： https://www.cnblogs.com/lnice/p/9668750.html

https://www.jianshu.com/p/0d35521bcf2d
https://blog.csdn.net/m0_38003171/article/details/79760487?utm_source=blogxgwz9

聊聊storm的direct grouping
https://blog.csdn.net/weixin_34099526/article/details/88718954?utm_medium=distribute.pc_relevant_bbs_down.none-task-blog-baidujs-1.nonecase&depth_1-utm_source=distribute.pc_relevant_bbs_down.none-task-blog-baidujs-1.nonecase

storm与spring结合开发 spring要单例   打包需要注意公共的jar冲突
https://my.oschina.net/chuibilong/blog/802053

kafka 重启consumer 重复消费问题
https://www.cnblogs.com/lshan/p/12573631.html